import * as Koa from 'koa';
import { config } from './config';
import * as winston from 'winston';
import * as appRoot from 'app-root-path'
// define the custom settings for each transport (file, console)
const options = {
  file: {
    level: 'info',
    filename: `${appRoot}/logs/app.log`,
    handleExceptions: true,
    json: true,
    maxsize: 5242880, // 5MB
    maxFiles: 5,
    colorize: false,
  },
  console: {
    format: winston.format.combine(
      winston.format.colorize(),
      winston.format.simple()
    ),
    level: 'debug',
    handleExceptions: true,
    json: true,
    colorize: true,
  },
};
export function logger (winstonInstance) {
  return async (ctx: Koa.Context, next: () => Promise<any>) => {

    const start = new Date().getMilliseconds();

    await next();

    const ms = new Date().getMilliseconds() - start;

    let logLevel: string;
    if (ctx.status >= 500) {
      logLevel = 'error';
    }
    if (ctx.status >= 400) {
      logLevel = 'warn';
    }
    if (ctx.status >= 100) {
      logLevel = 'info';
    }

    const msg: string = `${ctx.method} ${ctx.originalUrl} ${ctx.status} ${ms}ms`;

    winstonInstance.configure({
      level: config.debugLogging ? 'debug' : 'info',
      transports: [
        //
        // - Write to all logs with specified level to console.
        new winston.transports.Console(options.console)
        //
        // - Write all logs error (and below) to `error.log`.
        // new winston.transports.File(options.file)
      ]
    });

    winstonInstance.log(logLevel, msg);
  };
}